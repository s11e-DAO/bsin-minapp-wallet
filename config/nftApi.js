const http = uni.$u.http

// 商品列表
export const getNftPageList = (data) => {
	// 页数
	let {
		pageNo,
		pageSize,
		type,
		tenantId,
	} = data
	return http.post('', {
		serviceName: 'GoodsService',
		methodName: 'getPageList',
		bizParams: {
			type,
			tenantId,
		},
		pagination: {
			pageNum: pageNo,
			pageSize
		}
	})
}

// 商品详情
export const getGoodsDetails = (data) => {
	return http.post('', {
		serviceName: 'GoodsService',
		methodName: 'getGoods',
		bizParams: {
			...data
		}
	})
}

// 创建购买订单
export const createPurchaseOrder = (data) => {
	return http.post('', {
		serviceName: 'PurchaseService',
		methodName: 'createPurchaseOrder',
		bizParams: {
			...data
		}
	})
}

// 领取
export const receiveNftTheme = (data) => {
	return http.post('', {
		serviceName: 'NftThemeService',
		methodName: 'receive',
		bizParams: {
			...data
		}
	})
}

// daoNFT身份
export const getPositionByTenantId = (data) => {
	let {
		pageNo,
		pageSize,
		tenantId
	} = data
	return http.post('', {
		serviceName: 'NftPositionService',
		methodName: 'getPositionByTenantId',
		bizParams: {
			tenantId,
		},
		pagination: {
			pageNum: pageNo,
			pageSize
		}
	})
}

// daoNFT详情
export const getPositionDetails = (data) => {
	return http.post('', {
		serviceName: 'NftPositionService',
		methodName: 'getPosition',
		bizParams: {
			...data
		}
	})
}

// 我的

// 我的身份
export const getMyPositions = (data) => {
	// 页数
	let {
		pageNo,
		pageSize,
		customerNo
	} = data
	return http.post('', {
		serviceName: 'NftPositionService',
		methodName: 'getMyPositions',
		bizParams: {
			customerNo
		},
		pagination: {
			pageNum: pageNo,
			pageSize
		}
	})
}

// 我的nft 碎片 主题
export const getMyNftThemes = (data) => {
	// 页数
	let {
		pageNo,
		pageSize,
		customerNo,
		type
	} = data
	return http.post('', {
		serviceName: 'NftThemeService',
		methodName: 'getMyNftThemes',
		bizParams: {
			customerNo,
			type
		},
		pagination: {
			pageNum: pageNo,
			pageSize
		}
	})
}

// 我的nft详情
export const getMyNft = (data) => {
	return http.post('', {
		serviceName: 'NftThemeService',
		methodName: 'getMyNft',
		bizParams: {
			...data
		}
	})
}

// 转让NFT
export const giveAway = (data) => {
	return http.post('', {
		serviceName: 'NftThemeService',
		methodName: 'giveAway',
		bizParams: {
			...data
		}
	})
}
