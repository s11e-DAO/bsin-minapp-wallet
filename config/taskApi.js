const http = uni.$u.http

// 创建任务
export const createTask = (data) => {
	return http.post('', {
		serviceName: 'TaskService',
		methodName: 'createTask',
		bizParams: {
			...data
		}
	})
}

/**
 * 数据列表展示
 */

// 任务列表数据展示
export const getTaskPageList = (data) => {
	// 页数
	let {
		pageNo,
		pageSize,
		status
	} = data
	return http.post('', {
		serviceName: 'TaskService',
		methodName: 'getPageList',
		bizParams: {
			status
		},
		pagination: {
			pageNum: pageNo,
			pageSize
		}
	})
}

// 提案数据列表展示
export const getVoteProposalPageList = (data) => {
	// 页数
	let {
		pageNo,
		pageSize,
		status
	} = data
	return http.post('', {
		serviceName: 'VoteProposalService',
		methodName: 'getPageList',
		bizParams: {
			status
		},
		pagination: {
			pageNum: pageNo,
			pageSize
		}
	})
}

// 选举列表展示
export const getVoteElectionServicePageList = (data) => {
	// 页数
	let {
		pageNo,
		pageSize,
		type
	} = data
	return http.post('', {
		serviceName: 'VoteElectionService',
		methodName: 'getPageList',
		bizParams: {
			type
		},
		pagination: {
			pageNum: pageNo,
			pageSize
		}
	})
}

/**
 * 查询详情
 */

// 查询任务详情
export const getTaskDetails = (data) => {
	return http.post('', {
		serviceName: 'TaskService',
		methodName: 'getTaskDetails',
		bizParams: {
			...data
		}
	})
}

// 查询提案详情
export const getVoteProposalDetails = (data) => {
	return http.post('', {
		serviceName: 'VoteProposalService',
		methodName: 'getProposal',
		bizParams: {
			...data
		}
	})
}

// 查看选举投票详情
export const getVoteElectionOption = (data) => {
	return http.post('', {
		serviceName: 'VoteElectionService',
		methodName: 'getVoteElectionOption',
		bizParams: {
			...data
		}
	})
}


// 领取任务
export const claimTask = (data) => {
	return http.post('', {
		serviceName: 'TaskService',
		methodName: 'claimTask',
		bizParams: {
			...data
		}
	})
}

// 提案支持反对
export const voteApi = (data) => {
	return http.post('', {
		serviceName: 'VoteProposalService',
		methodName: 'vote',
		bizParams: {
			...data
		}
	})
}
// 评选选举
export const votesApi = (data) => {
	return http.post('', {
		serviceName: 'VoteElectionService',
		methodName: 'vote',
		bizParams: {
			...data
		}
	})
}

// 我的任务列表数据展示
export const getMyTaskPageList = (data) => {
	// 页数
	let {
		pageNo,
		pageSize,
		customerNo
	} = data
	return http.post('', {
		serviceName: 'TaskService',
		methodName: 'getPageList',
		bizParams: {
			customerNo
		},
		pagination: {
			pageNum: pageNo,
			pageSize
		}
	})
}

// 我认领的任务
export const getClaimedTasks = (data) => {
	console.log('我认领的任务', data)
	// 页数
	let {
		pageNo,
		pageSize,
		customerNo
	} = data
	return http.post('', {
		serviceName: 'TaskService',
		methodName: 'getClaimedTasks',
		bizParams: {
			customerNo
		},
		pagination: {
			pageNum: pageNo,
			pageSize
		}
	})
}

// 发布任务
export const publishTask = (data) => {
	return http.post('', {
		serviceName: 'TaskService',
		methodName: 'publishTask',
		bizParams: {
			...data
		}
	})
}

// 提交任务
export const submitTask = (data) => {
	return http.post('', {
		serviceName: 'TaskService',
		methodName: 'submitTask',
		bizParams: {
			...data
		}
	})
}

// 审核任务
export const auditTask = (data) => {
	return http.post('', {
		serviceName: 'TaskService',
		methodName: 'auditTask',
		bizParams: {
			...data
		}
	})
}
