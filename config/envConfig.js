!(function () {
    var envConfig = {
		appid: 'wx581d0c32a8c783a1',
		// 重定向地址
		wxRedirectUrl: 'http://h5.huoyuanshequ.com/#/pages/userCenter/userCenter',
		// 接口请求地址
		// 线上
		// baseURL: 'http://114.116.93.253:8097/biz-gateway',
		// 本地
		baseURL: 'http://127.0.0.1:8097/biz-gateway',
		/**
		 * 租户ID
		 * 目前用到地方:1登陆2小人物查询dao信息
		 */
		tenantId: "1542034501822189568",
		ccy: "fs",
		ccyName: "火源"
    };
    module.exports = envConfig;
})();
