const http = uni.$u.http

// 我的订单
export const getOrderPageList = (data) => {
	// 页数
	let {
		pageNo,
		pageSize,
		orderType,
		customerNo,
		tenantId
	} = data
	return http.post('', {
		serviceName: 'OrderService',
		methodName: 'getPageList',
		bizParams: {
			orderType,
			customerNo,
			tenantId
		},
		pagination: {
			pageNum: pageNo,
			pageSize
		}
	})
}
