const http = uni.$u.http

// 暂时废弃H5
// 注册
// export const register = (data) => {
// 	return http.post('', {
// 		serviceName: 'HysqCustomerService',
// 		methodName: 'register',
// 		bizParams: {
// 			...data
// 		}
// 	})
// }
// 登陆
// export const login = (data) => {
// 	return http.post('', {
// 		serviceName: 'HysqCustomerService',
// 		methodName: 'login',
// 		bizParams: {
// 			...data
// 		}
// 	})
// }
// 微信授权登陆
export const getUserInfo = (data) => {
	return http.post('', {
		serviceName: 'CustomerService',
		methodName: 'getUserInfo',
		bizParams: {
			...data
		}
	})
}
