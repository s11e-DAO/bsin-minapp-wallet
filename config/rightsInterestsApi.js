const http = uni.$u.http


// 权益列表
export const getRightsInterestsPageList = (data) => {
	// 页数
	let {
		pageNo,
		pageSize
	} = data
	return http.post('', {
		serviceName: 'RightsInterestsService',
		methodName: 'getPageList',
		bizParams: {
			// ...data
		},
		pagination: {
			pageNum: pageNo,
			pageSize
		}
	})
}

// 兑换权益
export const exchangeIn = (data) => {
	return http.post('', {
		serviceName: 'RightsInterestsService',
		methodName: 'exchangeIn',
		bizParams: {
			...data
		}
	})
}
// 我的权益
export const getMyRightsInterests = (data) => {
	// 页数
	let {
		pageNo,
		pageSize,
		customerNo
	} = data
	return http.post('', {
		serviceName: 'RightsInterestsService',
		methodName: 'getMyRightsInterests',
		bizParams: {
			customerNo
		},
		pagination: {
			pageNum: pageNo,
			pageSize
		}
	})
}

// 权益详情
export const getRightsInterestsDetails = (data) => {
	return http.post('', {
		serviceName: 'RightsInterestsService',
		methodName: 'getRightsInterests',
		bizParams: {
			...data
		}
	})
}
