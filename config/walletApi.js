const http = uni.$u.http

// 开通钱包
export const openWallet = (data) => {
	return http.post('', {
		serviceName: 'CustomerService',
		methodName: 'openWallet',
		bizParams: {
			...data
		}
	})
}

// 查看账户详情
export const getWalletDetail = (data) => {
	return http.post('', {
		serviceName: 'AccountService',
		methodName: 'getAccountByCustomerNoAndCcy',
		bizParams: {
			...data
		}
	})
}


// 转账
export const transferAccounts = (data) => {
	return http.post('', {
		serviceName: 'AccountService',
		methodName: 'transfer',
		bizParams: {
			...data
		}
	})
}
