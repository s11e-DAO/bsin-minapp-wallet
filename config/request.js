// 此vm参数为页面的实例，可以通过它引用vuex中的变量
// const baseURL = getApp().globalData.baseURL
const envConfig = require('./envConfig.js')
module.exports = (vm) => {
	// 初始化请求配置
	/* config 为默认全局配置*/
	uni.$u.http.setConfig((config) => {
		/* 根域名 */
		// config.baseURL = 'http://114.116.93.253:32275/biz-gateway';
		config.baseURL = envConfig.baseURL
		return config
	})

	// 请求拦截
	uni.$u.http.interceptors.request.use((config) => {
		// 获取token
		let token = uni.getStorageSync('token')
		config.header.Authorization = token
		return config
	}, (config) => {
		console.log('error')
		return config
	})

	// 响应拦截 response拦截器, 处理response
	uni.$u.http.interceptors.response.use((response) => {
		// console.log('response', response)
		const data = response.data
		if (response.statusCode === 200) {
			if (data.code !== "000000") {
				// 错误时的信息提示，来自后台
				uni.$u.toast(data.message)
				return false
			}
			return data === undefined ? {} : data
		}
	}, (response) => {
		// 对响应错误做点什么 （statusCode !== 200）
		return response
	})
}
