const http = uni.$u.http

// 广场

// 获取dao类型
export const getDictItemPageList = (data) => {
	return http.post('', {
		serviceName: 'DictService',
		methodName: 'getDictItemPageList',
		bizParams: {
			...data
		},
		pagination: {
			pageNum: 1,
			pageSize: 10
		}
	})
}
// 查询所有dao
export const getDaoList = (data) => {
	let {
		pageNo,
		pageSize,
		type
	} = data
	return http.post('', {
		serviceName: 'AdminTenantDaoService',
		methodName: 'getPageList',
		bizParams: {
			type
		},
		pagination: {
			pageNum: pageNo,
			pageSize
		}
	})
}

// 查询dao详情
export const getTenantDao = (data) => {
	return http.post('', {
		serviceName: 'AdminTenantDaoService',
		methodName: 'getTenantDao',
		bizParams: {
			...data
		}
	})
}

// 查询dao详情
export const getMyTenantDao = (data) => {
	return http.post('', {
		serviceName: 'TenantDaoService',
		methodName: 'getMyTenantDao',
		bizParams: {
			...data
		}
	})
}

// 获取微信签名
export const getWechatSign = (data) => {
	return http.post('', {
		serviceName: 'WechatService',
		methodName: 'createJsapiSignature',
		bizParams: {
			...data
		}
	})
}
